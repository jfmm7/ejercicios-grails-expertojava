<%@ page import="es.ua.expertojava.todo.User" %>
<div id="menu">
    <nobr>
        <sec:ifLoggedIn>
            <sec:username/> |
           <g:remoteLink class="logout" controller="logout" method="post" asynchronous="false" onSuccess="location.reload()">Logout</g:remoteLink>
        </sec:ifLoggedIn>
        <sec:ifNotLoggedIn>
            <g:link controller="user" action="login">Login</g:link>
        </sec:ifNotLoggedIn>
    </nobr>
</div>
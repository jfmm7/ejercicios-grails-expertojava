class UrlMappings {

	static mappings = {
        "/$controller/$action?/$id?(.$format)?"{
            constraints {
                // apply constraints here
            }
        }

        "/"(view:"/index")

        "/api/todos"(resources:"todo")
        "/api/tags"(resources:"tag")
        "/api/categories"(resources:"category")

        "500"(view:'/error')
        "404"(view:'/error')
        "/todos/$username"(controller:"todo",action:"showtodosbyuser")
	}
}

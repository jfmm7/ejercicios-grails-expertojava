import es.ua.expertojava.todo.*

class BootStrap {

    def init = { servletContext ->
        def categoryHome = new Category(name:"Hogar").save()
        def categoryJob = new Category(name:"Trabajo").save()

        def tagEasy = new Tag(name:"Fácil").save()
        def tagDifficult = new Tag(name:"Difícil").save()
        def tagArt = new Tag(name:"Arte").save()
        def tagRoutine = new Tag(name:"Rutina").save()
        def tagKitchen = new Tag(name:"Cocina").save()

        def todoPaintKitchen = new Todo(title:"Pintar cocina", date:new Date()+1, realizada:false)
        def todoCollectPost = new Todo(title:"Recoger correo postal", date:new Date()+2, realizada:false)
        def todoBakeCake = new Todo(title:"Cocinar pastel", date:new Date()+4, realizada:false)
        def todoWriteUnitTests = new Todo(title:"Escribir tests unitarios", date:new Date(),realizada:false)


        todoCollectPost.addToTags(tagRoutine)
        todoCollectPost.category = categoryJob
        todoCollectPost.save()


        todoWriteUnitTests.addToTags(tagEasy)
        todoWriteUnitTests.category = categoryJob
        todoWriteUnitTests.save()

        Role role1 = new Role(authority: "ROLE_ADMIN").save()
        Role role2 = new Role(authority: "ROLE_BASIC").save()

        User admin = new User(username: "admin", password: "admin", name:"Admin", surnames:"Admin", email:"admin@admin.com").save()
        User basic1 = new User(username: "usuario1", password: "usuario1", name:"Juan", surnames:"Munoz", email:"usu1@todo.com").save()
        User basic2 = new User(username: "usuario2", password: "usuario2", name:"Pedro", surnames:"Martin", email:"usua2@todo.com").save()

        PersonRole.create admin, role1
        PersonRole.create basic1, role2
        PersonRole.create basic2, role2

        todoPaintKitchen.addToTags(tagDifficult)
        todoPaintKitchen.addToTags(tagArt)
        todoPaintKitchen.addToTags(tagKitchen)
        todoPaintKitchen.category = categoryHome
        todoPaintKitchen.user = basic1
        todoPaintKitchen.save()

        todoBakeCake.addToTags(tagEasy)
        todoBakeCake.addToTags(tagKitchen)
        todoBakeCake.category = categoryHome
        todoBakeCake.user = basic2
        todoBakeCake.save()

    }
    def destroy = { }
}

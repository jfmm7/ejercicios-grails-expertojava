package es.ua.expertojava.todo

import grails.transaction.Transactional

class CategoryService {

    def quitarTodos(Category categoryInstance) {
        def todos = categoryInstance.todos.findAll()
        todos.forEach{
            todo ->
                categoryInstance.removeFromTodos(todo)
        }
    }
}

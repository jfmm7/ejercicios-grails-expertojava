package es.ua.expertojava.todo

import grails.transaction.Transactional
import groovy.time.TimeCategory

class TodoService {

    def getNextTodos(Integer days, params) {
        Date now = new Date(System.currentTimeMillis())
        Date to = now + days
        println(params)
        Todo.findAllByDateBetween(now, to, params)
    }

    def countNextTodos(Integer days) {
        Date now = new Date(System.currentTimeMillis())
        Date to = now + days
        Todo.countByDateBetween(now,to)
    }

    def saveTodo(Todo todoInstance){
        if(todoInstance.dateDone == null && todoInstance.realizada == true)
        {
            todoInstance.dateDone = new Date(System.currentTimeMillis());
        }
    }

    def lastTodosDone(Integer horas){
        Date now = new Date()
        Date anterior
        use(TimeCategory) {
            anterior = now - horas.hours
        }
        Todo.findAllByDateDoneBetween(anterior,now)

    }

    def getByCategory(){

    }
}

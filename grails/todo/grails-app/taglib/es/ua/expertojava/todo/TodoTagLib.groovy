package es.ua.expertojava.todo

class TodoTagLib {
    //static defaultEncodeAs = [taglib:'html']
    static encodeAsForTags = [TodoTagLib: [taglib:'none']]
    //static encodeAsForTags = [tagName: [taglib:'html'], otherTagName: [taglib:'none']]

    static namespace = 'me'

    def includeJs = {attrs ->
        out << "<script src='scripts/${attrs['script']}.js'></script>"
    }

    def renderImage = { attrs ->
        //println "Renderizar la imagen ${attrs.image}"
        asset.image(src: attrs.image)
    }

    def esAdmin = {attrs, body ->
        def usuario = attrs['usuario']
        if(usuario != null && usuario.tipo == "administrador"){
            out << body()
        }
    }

    def printLink = {attr, body ->
        def mkp = new groovy.xml.MarkupBuilder(out)
        mkp.a(href:body(),body())
    }
}

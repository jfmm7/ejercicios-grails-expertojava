package es.ua.expertojava.todo

class PrintIconFromBooleanTagLib {
    //static defaultEncodeAs = [taglib:'html']
    static encodeAsForTags = [PrintIconFromBooleanTagLib: [taglib:'none']]
    static namespace = 'todo'

    def printIconFromBoolean = { attrs ->
        //println "Renderizar estado de Tarea ${attrs}"
        if(attrs['value'])
            out << asset.image(src:"iconDone.png")
        else
            out << asset.image(src:"iconLeft.png")
    }
}

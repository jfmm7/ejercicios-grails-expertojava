package es.ua.expertojava.todo

class Todo {
    String title
    String description
    Date date
    Date reminderDate
    String url
    Category category
    Boolean realizada

    Date dateDone

    Date dateCreated
    Date lastUpdated

    User user

    static searchable = [only: ['title','description']]

    static hasMany = [tags:Tag]



    static constraints = {
        title(blank:false)
        description(blank:true, nullable:true, maxSize:1000)
        date(nullable:false,
            validator: {
                if(it){
                    return it.after(new Date())
                }
                return true
            }
        )
        reminderDate(nullable:true,
            validator: {val,obj ->
                if(val && obj.date)
                {
                    return val.before(obj.date)
                }
                return true
            }
        )
        url(nullable:true, url:true)
        category(nullable:true)
        realizada(nullable:false, default:false)
        dateDone(nullable:true)
        user(nullable:true)
    }

    String toString(){
        title
    }
}
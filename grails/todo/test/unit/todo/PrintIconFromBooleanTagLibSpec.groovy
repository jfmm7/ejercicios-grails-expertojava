package todo

import es.ua.expertojava.todo.PrintIconFromBooleanTagLib
import grails.test.mixin.TestFor
import spock.lang.Specification

/**
 * See the API for {@link grails.test.mixin.web.GroovyPageUnitTestMixin} for usage instructions
 */
@TestFor(PrintIconFromBooleanTagLib)
class PrintIconFromBooleanTagLibSpec extends Specification {

    def setup() {
    }

    def cleanup() {
    }

    void "test something"() {
    }
}

package es.ua.expertojava.todo

import asset.pipeline.grails.AssetsTagLib
import es.ua.expertojava.todo.PrintIconFromBooleanTagLib
import grails.test.mixin.TestFor
import spock.lang.Specification
import spock.lang.Unroll

/**
 * See the API for {@link grails.test.mixin.web.GroovyPageUnitTestMixin} for usage instructions
 */
@TestFor(PrintIconFromBooleanTagLib)
class PrintIconFromBooleanTagLibSpec extends Specification {

    def setup() {
    }

    def cleanup() {
    }

    @Unroll
    void "El método printIconFromBoolean devuelve una ruta a una imagen"() {
        given:
        def assetsTagLib = Mock(AssetsTagLib)
        tagLib.metaClass.asset = assetsTagLib
        when:
        def output = applyTemplate('<todo:printIconFromBoolean value="${value}" />', [value:value])
        then:
        output == expectedOutput
        and:
        1 * assetsTagLib.image(_) >> { value ? "iconDone.png" : "iconLeft.png" }
        where:
        value   |   expectedOutput
        true    |   "iconDone.png"
        false   |   "iconLeft.png"
    }
}

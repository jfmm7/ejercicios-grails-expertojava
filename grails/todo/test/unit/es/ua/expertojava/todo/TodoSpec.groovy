package es.ua.expertojava.todo

import grails.test.mixin.TestFor
import spock.lang.Specification

/**
 * See the API for {@link grails.test.mixin.domain.DomainClassUnitTestMixin} for usage instructions
 */
@TestFor(Todo)
class TodoSpec extends Specification {

    def setup() {
    }

    def cleanup() {
    }

    void "Comprobar restriccion de fecha de recordatorio de tarea"() {

        when:"Fecha recordatorio posterior a fecha de tarea NO GUARDAR"
        new Todo(title:"Nueva Tarea",date:new Date()+4,reminderDate: new Date()+5,realizada: false).save(flush: true)
        then:
        Todo.count() == 0

        when:"Fecha recordatorio anterior a fecha de tarea"
            new Todo(title:"Nueva Tarea",date:new Date()+4,reminderDate: new Date()+1,realizada: false).save(flush: true)
        then:
            Todo.count() == 1

    }
}

package es.ua.expertojava.todo

import grails.test.mixin.TestFor
import spock.lang.Specification

/**
 * See the API for {@link grails.test.mixin.web.GroovyPageUnitTestMixin} for usage instructions
 */
@TestFor(TodoTagLib)
class TodoTagLibSpec extends Specification {

    def setup() {
    }

    def cleanup() {
    }

    void "La etiqueta includeJs devuelve una referencia a la librería javascript pasada por parámetro"() {
        expect:
        applyTemplate('<me:includeJs script="" />') == "<script src='scripts/.js'></script>"
        applyTemplate('<me:includeJs script="myfile" />') == "<script src='scripts/myfile.js'></script>"
    }

    void "El pie de página se renderiza correctamente"() {
        when:
        def result = render(template: '/common/footer')

        then:
        result == "<div class=\"footer\" role=\"contentinfo\">\n" +
                "&copy; 2015 Experto en Desarrollo de Aplicaciones Web con JavaEE y Javascript<br/>\n" +
                "    Aplicación Todo creada por Juan Francisco Muñoz Martínez (48474073D)\n" +
                "</div>"
    }
}

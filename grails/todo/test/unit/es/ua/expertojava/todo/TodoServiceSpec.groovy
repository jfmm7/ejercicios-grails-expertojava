package es.ua.expertojava.todo

import grails.test.mixin.TestFor
import spock.lang.Specification

/**
 * See the API for {@link grails.test.mixin.services.ServiceUnitTestMixin} for usage instructions
 */
@TestFor(TodoService)
class TodoServiceSpec extends Specification {

    def setup() {
    }

    def cleanup() {
    }

    void "El método getNextTodos devuelve los siguientes todos de los días pasado por parámetro"() {
        given:
        def todoDayBeforeYesterday = new Todo(title:"Todo day before yesterday", date: new Date() - 2, realizada:false )
        def todoYesterday = new Todo(title:"Todo yesterday", date: new Date() - 1 ,realizada:false)
        def todoToday = new Todo(title:"Todo today", date: new Date(),realizada:false)
        def todoTomorrow = new Todo(title:"Todo tomorrow", date: new Date() + 1 ,realizada:false)
        def todoDayAfterTomorrow = new Todo(title:"Todo day after tomorrow", date: new Date() + 2 ,realizada:false)
        def todoDayAfterDayAfterTomorrow = new Todo(title:"Todo day after tomorrow", date: new Date() + 3 ,realizada:false)
        and:
        mockDomain(Todo,[todoDayBeforeYesterday, todoYesterday, todoToday, todoTomorrow, todoDayAfterTomorrow, todoDayAfterDayAfterTomorrow])
        and:
        def nextTodos = service.getNextTodos(2,[:])
        expect:
        Todo.count() == 3
        and:
        nextTodos.containsAll([todoTomorrow, todoDayAfterTomorrow])
        nextTodos.size() == 2
        and:
        !nextTodos.contains(todoDayBeforeYesterday)
        !nextTodos.contains(todoToday)
        !nextTodos.contains(todoYesterday)
        !nextTodos.contains(todoDayAfterDayAfterTomorrow)
    }

    void "El metodo countNextTodos devuelve las tereas desde hoy hasta el numero de dias pasado"(){
        given:
            def dia1 = new Todo(title:"tarea dia 1", date: new Date() + 1 ,realizada:false)
            def dia2 = new Todo(title:"tarea dia 2", date: new Date() + 2 ,realizada:false)
            def dia3 = new Todo(title:"tarea dia 3", date: new Date() + 3 ,realizada:false)
            def dia4 = new Todo(title:"tarea dia 4", date: new Date() + 3 ,realizada:false)
        and:
            mockDomain(Todo,[dia1, dia2, dia3, dia4])
        and:
            def todos = service.countNextTodos(2)
        expect:
            todos == 2

    }

    void "El metodo saveTodo comprueba que al guardar todo con realizada false no genera fecha de realizacion"()
    {
        given:
            def dia1 = new Todo(title:"tarea dia 1", date: new Date() + 1 ,realizada:false)
        and:
            mockDomain(Todo,[dia1])
        and:
            service.saveTodo(dia1)
        expect:
            dia1.dateDone == null
    }

    void "El metodo saveTodo comprueba que al guardar todo con realizada true genera fecha de realizacion"()
    {
        given:
            def dia2 = new Todo(title:"tarea dia 2", date: new Date() + 1 ,realizada:true)
        and:
            mockDomain(Todo,[dia2])
        and:
            service.saveTodo(dia2)
        expect:
            dia2.dateDone == new Date()
    }

}

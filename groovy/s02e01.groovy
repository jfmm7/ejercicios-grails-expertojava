//def factorial
factorial = { it>1 ? it * factorial(it - 1) : 1 }

/* Añade aquí la implementación del factorial en un closure */

assert factorial(1)==1
assert factorial(2)==1*2
assert factorial(3)==1*2*3
assert factorial(4)==1*2*3*4
assert factorial(5)==1*2*3*4*5
assert factorial(6)==1*2*3*4*5*6
assert factorial(7)==1*2*3*4*5*6*7
assert factorial(8)==1*2*3*4*5*6*7*8
assert factorial(9)==1*2*3*4*5*6*7*8*9
assert factorial(10)==1*2*3*4*5*6*7*8*9*10

/* Añade a partir de aquí el resto de tareas solicitadas en el ejercicios */

lista = (1..5).toList();
lista.each {
    println "Factorial($it) = "+factorial(it) // `it` is an implicit parameter corresponding to the current element
}

ayer = {
    return it-1
}

mañana = {
    return it+1
}

println ayer(new Date())
println mañana(new Date())

hoy = new Date()
dias6 = hoy+6;

fechas = (hoy..dias6).toList();
fechas.each{
    println "Dia anterior a ($it) : "+ayer(it)+"- Dia posterior a ($it) : "+mañana(it)
}
return

